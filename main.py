from PyPDF2 import PdfReader
from PyPDF2.errors import PdfReadError
from gtts import gTTS, gTTSError


# Convert PDF to Text
def pdf2text(filename):
	extracted_text = ""
	try:
		reader = PdfReader(filename)
	except FileNotFoundError:
		print("File not found.")
		return False
	except PdfReadError:
		print("File is invalid.")
		return False
	else:
		for page in reader.pages:
			extracted_text += page.extract_text()
			print("Pdf converted to text.")
		return extracted_text


# Convert Text to Speech
def text2speech(text):
	language = 'en'
	try:
		tts = gTTS(text=text, lang=language, slow=False)
	except AssertionError:
		print("Text it empty.")
	except ValueError or RuntimeError:
		print("Lang not supported or load error.")
	else:
		try:
			tts.save("output.mp3")
		except gTTSError:
			print("Error with saving audio.")
		else:
			print("Audio saved.")


# Start
def start():
	filename = input("Enter the pdf name: ")
	text = pdf2text(filename)
	if text:
		text2speech(text)


start()
